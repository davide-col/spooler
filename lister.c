#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <getopt.h>
#include <libgen.h>
#include <time.h>
#include <sys/wait.h>
#define SPOOLER "./Spooler"

char * PROJETSE;

//Définit le chemin du Spooler
//Entrée : void
//Sortie : void
void defineSpooler(){
  if (getenv("PROJETSE")==NULL){
    PROJETSE=SPOOLER;
  }
  else{
    PROJETSE=getenv("PROJETSE");
  }
}

//Retourne le chemin du fichier verrou
//Entrée : void
//Sortie : String
char * verrouPath(){
	char * resultat=malloc(1000*sizeof(char));
	strcpy(resultat,PROJETSE);
	strcat(resultat,"/verrou");
	return resultat;
}

//Verrouille le fichier verrou
//Entrée : void
//Sortie : void
void verrouille(){
  char * verroup = verrouPath();
  FILE * file = fopen(verroup,"w");
  if (file == NULL) {
			perror("Failed to open file : verrou");
			exit (1);
	}
  int fd = fileno(file);
  lockf(fd,F_TLOCK,0);
  fclose(file);
  free(verroup);
	return;
}

//Déverrouille le fichier verrou
//Entrée : void
//Sortie : void
void deverrouille(){
  char * verroup = verrouPath();
  FILE * file = fopen(verroup,"w");
  if (file == NULL) {
			perror("Failed to open file : verrou");
			exit (1);
	}
  int fd = fileno(file);
  lockf(fd,F_ULOCK,0);
  fclose(file);
  free(verroup);
  return;
}

//Test si le fichier verrou est vérrouillé
//Entrée : void
//Sortie : int, retourne -1 si vérrouillé et 0 sinon
int estVerrouille(){
char * verroup = verrouPath();
  FILE * file = fopen(verroup,"w");
  if (file == NULL) {
			perror("Failed to open file : verrou");
			exit (1);
	}
  int fd = fileno(file);
  int resultat=lockf(fd,F_TEST,0);
  free(verroup);
  fclose(file);
  return resultat;
}

//Récupère les informations dans le fichier jJob
//Entrée : fichier jJob contenant les informations et ligne à extraire
//Sortie : String de la ligne extraite
char * getInfo(char * filepath, int line){
    int end, loop;
    char str[512];

    FILE *fd = fopen(filepath, "r");
    if (fd == NULL) {
        perror("Failed to open file : jJob");
        exit (1);
    }

    for(end = loop = 0;loop<line;++loop){
        if(0==fgets(str, sizeof(str), fd)){//include '\n'
            end = 1;//can't input (EOF)
            break;
        }
    }
    if(!end)
    fclose(fd);
		char * resultat=malloc(1000*sizeof(char));
		strcpy(resultat,str);
    size_t len = strlen(resultat);
    while (len > 0){
      if (resultat[len] == '\n') {
        resultat[len] = '\0';
      }
      len--;
    }
    return resultat;
}

//Renvoie le chemin d'un fichier dans le Spooler
//Entrée : String, nom du fichier
//Sortie : String, chemin du fichier
char * path (char * s){
  char * resultat = malloc(1000*sizeof(char));
	strcpy(resultat,PROJETSE);
  strcat(resultat,"/");
  strcat(resultat,s);
  return resultat;
}

//Renvoie le chemin du jJob à partir du chemin dJob
//Entrée : chemin dJob
//Sortie : chemin jJob
char * d2jJob(char * dJobPath){
	char * name = malloc(1000*sizeof(char));
  strcpy(name,basename(dJobPath));
	char * jJobPath = malloc(1000*sizeof(char));
	name[0]='j';
  strcpy(jJobPath,PROJETSE);
	strcat(jJobPath,"/");
	strcat(jJobPath,name);
  free(name);
  return jJobPath;
}

//Vérifie si le Spooler nous appartient
//Entrée : void
//Sortie : int, 1 s'il nous appartient et 0 sinon
int propSpooler (){
  struct stat info;
	stat(PROJETSE, &info);
  return (info.st_uid==getuid());
}

//Test si le fichier jJob existe
//Entrée : chemin supposé de jJob
//Sortie : int, 1 s'il existe et 0 sinon
int jobExiste (char * job){
  FILE * file = fopen(job,"r");
  if (file == NULL) {
    return 0;
	}
  else return 1;
}

//Fonction principale qui liste les fichiers déposés dans le Spooler
void lister(int lflag, int uflag, char * utilisateur) {
  static DIR * directory;
  static struct dirent *lecture;
	directory = opendir(PROJETSE);
  while ((lecture = readdir(directory))) { //parcours le repertoire courant
    if(strcmp(lecture->d_name, ".") != 0 && strcmp(lecture->d_name, "..") != 0){
      char * djobpath = path(lecture->d_name); //Chemin dJob
      char * jjobpath = d2jJob(djobpath); //Chemin jJob¨
      //Véreifie s'il existe des fichiers à compresser
      if((lecture->d_name[0]=='d')&&(jobExiste(jjobpath))){
				char * getInfo1 = getInfo(jjobpath,1);
				char * getInfo2 = getInfo(jjobpath,2);
				char * getInfo4 = getInfo(jjobpath,4);
				char * getInfo6 = getInfo(jjobpath,6);
				//Affichage les informations des fichiers dans le Spooler
        if (((uflag==1)&&(strcmp(utilisateur,getInfo1))==0)||(uflag==0)){
          printf("%s\t%s\t%s",getInfo2,getInfo1,getInfo6);
					//Affiche le nom du fichier
					if (lflag==1){
            printf("\t%s",getInfo4);
          }
          printf("\n");
        }
				free(getInfo1);
				free(getInfo2);
				free(getInfo4);
				free(getInfo6);
      }
      free(djobpath);
      free(jjobpath);
    }
  }
	closedir(directory);
}

int main(int argc, char * argv[]) {
	defineSpooler(); //On redéfinit le chemin du Spooler en cas de variable d'environnement PROJETSE
  int lflag = 0;
  int uflag = 0;
  char * utilisateur;
  int c;
  while ((c = getopt (argc, argv, "lu:"))!=-1){ //Capte les options (getopt)
      switch (c){
        case 'l':
          lflag = 1;
					//Verifie si on est bien le propriétaire du Spooler
          if (!propSpooler()){
            perror("Vous n'etez pas le propriétaire du Spooler");
            exit(1);
          }
          break;
        case 'u':
          uflag = 1;
					//Verifie si on est bien le propriétaire du Spooler
          if (!propSpooler()){
            perror("Vous n'etez pas le propriétaire du Spooler");
            exit(1);
          }
          utilisateur=optarg;
          break;
        default:
          fprintf (stderr, "Erreur de getopt\n");
	         exit(1);
      }
  }
  while (estVerrouille()==-1){ //Vérifie si le fichier verrou est verrouillé
    printf("Le Spooler est verouillé ! \n");
    sleep(3);
  }
  verrouille(); //Verrouille le fichier verrou
  lister(lflag,uflag,utilisateur); //Lance la fonction lister
  deverrouille(); //Déverrouille le fichier verrou
return 0;
}
