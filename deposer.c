#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <getopt.h>
#include <libgen.h>
#include <time.h>
#include <sys/wait.h>
#define SPOOLER "./Spooler"

char * PROJETSE;
uid_t suid;

#define BUFSIZE  1024
#define MONEOF (-1)


struct FICHIER {
	int fd;
	int writeonly;
	unsigned char buf[BUFSIZE];
	unsigned char *p;
	int taille;
};

struct FICHIER * monopen(char *nom, char *mode){
	int fd, modew;
	struct FICHIER *f;
	if (strcmp(mode,"r") == 0){

		fd = open (nom, O_RDONLY);
		if (fd < 0)
			return (NULL);
		modew = 0;
	}
	else if (strcmp(mode,"w") == 0){
		fd = open (nom, O_WRONLY | O_CREAT | O_TRUNC, 0777);
		if (fd < 0)
			return (NULL);
		modew = 1;
	}
	else{
		errno = EINVAL;
		return (NULL);
	}

	f = malloc (sizeof(struct FICHIER));
	f->taille = 0;
	f->p = f->buf;
	f->fd = fd;
	f->writeonly = modew;
		return (f);
}
static int flushbuf(struct FICHIER *f){
	if (write (f->fd, f->buf, f->taille) != f->taille){
		return (1);
	}

	f->taille = 0;
	f->p = f->buf;
	return (0);
}

int monclose(struct FICHIER *f){
	int ret = 0;
	if (f->writeonly){
		//flush buffer
		ret = flushbuf(f);
	}

	close (f->fd);
	free (f);
	return (ret);

}

int mongetchar(struct FICHIER *f){

	if (f->writeonly){
		// erreur : lecture dans un fichier ouvert en écriture
		errno = EINVAL;
		return (MONEOF);
	}
	if (f->taille == 0){
		f-> taille =  read(f->fd, f->buf, BUFSIZE );
		f->p = f->buf;
	}
	if (f->taille-- == 0)
		return (MONEOF);
	else
		return (*(f->p++));
}


int monputchar(struct FICHIER *f, unsigned char c){
	if (!f->writeonly){
		// erreur : lecture dans un fichier ouvert en écriture
		errno = EINVAL;
		return (MONEOF);
	}

	if (f->taille == BUFSIZE){
		//buffer plein !
		if (flushbuf(f))
			return (MONEOF);
	}
	f->taille ++;
	*f->p++ = c;
	return (c);
}

void copie (struct FICHIER * originalfile, char * fichier2){
		struct FICHIER *f1, *f2;
		int c;
		f1 = originalfile;
		f2 = monopen (fichier2,"w");

		if (!f1 || !f2){
			perror ("erreur ouverture");
			exit (1);
		}
		while (( c =mongetchar(f1)) != MONEOF)
			monputchar (f2,c);

		monclose (f1);
		monclose (f2);
    return;
}


//Définit le chemin du Spooler
//Entrée : void
//Sortie : void
void defineSpooler(){
  if (getenv("PROJETSE")==NULL){
    PROJETSE=SPOOLER;
  }
  else{
    PROJETSE=getenv("PROJETSE");
  }
}

//Retourne le chemin du fichier verrou
//Entrée : void
//Sortie : String
char * verrouPath(){
	char * resultat=malloc(1000*sizeof(char));
	strcpy(resultat,PROJETSE);
	strcat(resultat,"/verrou");
	return resultat;
}

//Verrouille le fichier verrou
//Entrée : void
//Sortie : void
void verrouille(){
  char * verroup = verrouPath();
  FILE * file = fopen(verroup,"w");
	if (file == NULL) {
			perror("Failed to open file : verrou");
			exit (1);
	}
  int fd = fileno(file);
  lockf(fd,F_TLOCK,0);
  fclose(file);
  free(verroup);
	return;
}

//Déverrouille le fichier verrou
//Entrée : void
//Sortie : void
void deverrouille(){
  char * verroup = verrouPath();
  FILE * file = fopen(verroup,"w");
	if (file == NULL) {
			perror("Failed to open file : verrou");
			exit (1);
	}
  int fd = fileno(file);
  lockf(fd,F_ULOCK,0);
  fclose(file);
  free(verroup);
  return;
}

//Test si le fichier verrou est vérrouillé
//Entrée : void
//Sortie : int, retourne -1 si vérrouillé et 0 sinon
int estVerrouille(){
char * verroup = verrouPath();
  FILE * file = fopen(verroup,"w");
  if (file == NULL) {
			perror("Failed to open file : verrou");
			exit (1);
	}
  int fd = fileno(file);
  int resultat=lockf(fd,F_TEST,0);
  free(verroup);
  fclose(file);
  return resultat;
}

//Donne l'identificateur pour les fichiers jJob
//Entrée : void
//Sortie : String chemin + nom
char * monMkstemp (){
  char * resultat = malloc(1000*sizeof(char));
  strcpy(resultat,"");
  strcat(resultat,PROJETSE);
	//version LINUX :
	strcat(resultat,"/jXXXXXX");
	//version Mac :
  //strcat(resultat,"/jXXXX");
  mkstemp(resultat);
  return resultat;
}

//Extrait l'identificateur d'un fichier jJob
//Entrée : String, chemin fichier Job
//Sortie : String, identificateur
char * identificateur(char * s){
	char * name = basename(s);
	name++;
	return name;
}

//Crée les informations (text) pour le fichier jJob
//Entrée : String chemin du fichier, String chemin de jJob
//Sortie : String contenant le text
char * createInfo (char * fichier, char * jJobPath){
  char * resultat = malloc(3000*sizeof(char));
  struct stat buf;
  stat (fichier,&buf);
  strcpy(resultat,getlogin());
  strcat(resultat,"\n");
	strcat(resultat,identificateur(jJobPath));
  strcat(resultat,"\n");
  char taille[2500];
	//Version Linux :
	sprintf(taille,"%ld",buf.st_size);
	//Version Mac :
  //sprintf(taille,"%lld",buf.st_size);
  strcat(resultat,taille);
  strcat(resultat,"\n");
  char * nomfichier = basename(fichier);
  strcat(resultat,nomfichier);
  strcat(resultat,"\n");
  strcat(resultat,fichier);
  strcat(resultat,"\n");
	time_t now;
	struct tm *curtime = localtime(&now);
  strcat(resultat,asctime(curtime));
  return resultat;
}

//Récupère les informations dans le fichier jJob
//Entrée : fichier jJob contenant les informations et ligne à extraire
//Sortie : String de la ligne extraite
char * getInfo(char * filepath, int line){
    int end, loop;
    char str[512];

    FILE *fd = fopen(filepath, "r");
    if (fd == NULL) {
        perror("Failed to open file : jJob");
        exit (1);
    }

    for(end = loop = 0;loop<line;++loop){
        if(0==fgets(str, sizeof(str), fd)){//include '\n'
            end = 1;//can't input (EOF)
            break;
        }
    }
    if(!end)
    fclose(fd);
		char * resultat=malloc(1000*sizeof(char));
		strcpy(resultat,str);
    size_t len = strlen(resultat);
    while (len > 0){
      if (resultat[len] == '\n') {
        resultat[len] = '\0';
      }
      len--;
    }
    return resultat;
}


//Retourne le chemin du dJob à partir du chemin jJob
//Entrée : chemin jJob
//Sortie : chemin dJob
char * j2dJob(char * jJobPath){
  char * name = malloc(1000*sizeof(char));
  strcpy(name,basename(jJobPath));
	char * dJobPath = malloc(1000*sizeof(char));
  strcpy(dJobPath,PROJETSE);
	name[0]='d';
	strcat(dJobPath,"/");
	strcat(dJobPath,name);
  free(name);
  return dJobPath;
}

//Renvoie l'ID d'un fichier
//Entrée : String, chemin du fichier
//Sortie : int, ID du fichier
int fileID (char * file){
  struct stat info;
  stat(file, &info);
  return info.st_uid;
}

//Fonction principale qui dépose les fichiers dans le Spooler
//Entrée : String, chemin du fichier à déposer
//Sortie : void
void deposer ( char * fichier){

  char * jJobPath = monMkstemp();
  //Changement d'UID
  suid=geteuid();
  seteuid(getuid());
  //
  struct FICHIER * originalfile = monopen (fichier,"r");
  //Recupereration de l'UID original
  seteuid(suid);
  //
  FILE * jfile = fopen(jJobPath,"w");
	if (jfile == NULL) {
			perror("Failed to open file : jJob");
			exit (1);
	}

	char * dJobPath = j2dJob(jJobPath);
  char * info = createInfo(fichier,jJobPath);
	copie(originalfile,dJobPath);
	fprintf(jfile,"%s",info);
  fclose(jfile);
  char * getInfo2=getInfo(jJobPath,2);
	printf("%s\n",getInfo2); //Affiche l'identificateur
  free(info);
  free(getInfo2);
  free(jJobPath);
	free(dJobPath);

  return;
}

//Renvoie l'ID du SpoolerID
//Entrée : void
//Sortie : void
void SpoolerID (){
  struct stat info;
  stat(PROJETSE, &info);
  printf("Spooler id : %d\n",info.st_uid);
}



int main(int argc, char const *argv[]) {
	defineSpooler(); //On redéfinit le chemin du Spooler en cas de variable d'environnement PROJETSE
	int i;
	if(argc==1){
		fprintf (stderr, "Veuillez insérer un argument.\n");
    exit(1);
	}
  for (i=1;i<=argc-1;i++){
		char * fullpath = realpath(argv[i],NULL); //chemin complet du fichier à déposer
		while (estVerrouille()==-1){ //Attente si Spooler vérrouillé
			perror("Le Spooler est verouillé !");
			sleep(3);
		}
		verrouille(); //Verrouille le Spooler
  	deposer(fullpath); //Dépose le fichier dans le Spooler
		deverrouille(); //Déverrouille le Spooler
		free(fullpath);
  }
  return 0;
}
