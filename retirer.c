#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <getopt.h>
#include <libgen.h>
#include <time.h>
#include <sys/wait.h>
#define SPOOLER "./Spooler"

char * PROJETSE;

//Définit le chemin du Spooler
//Entrée : void
//Sortie : void
void defineSpooler(){
  if (getenv("PROJETSE")==NULL){
    PROJETSE=SPOOLER;
  }
  else{
    PROJETSE=getenv("PROJETSE");
  }
}

//Retourne le chemin du fichier verrou
//Entrée : void
//Sortie : String
char * verrouPath(){
	char * resultat=malloc(1000*sizeof(char));
	strcpy(resultat,PROJETSE);
	strcat(resultat,"/verrou");
	return resultat;
}

//Verrouille le fichier verrou
//Entrée : void
//Sortie : void
void verrouille(){
  char * verroup = verrouPath();
  FILE * file = fopen(verroup,"w");
	if (file == NULL) {
			perror("Failed to open file : verrou");
			exit (1);
	}
  int fd = fileno(file);
  lockf(fd,F_TLOCK,0);
  fclose(file);
  free(verroup);
	return;
}

//Déverrouille le fichier verrou
//Entrée : void
//Sortie : void
void deverrouille(){
  char * verroup = verrouPath();
  FILE * file = fopen(verroup,"w");
	if (file == NULL) {
			perror("Failed to open file : verrou");
			exit (1);
	}
  int fd = fileno(file);
  lockf(fd,F_ULOCK,0);
  fclose(file);
  free(verroup);
  return;
}

//Test si le fichier verrou est vérrouillé
//Entrée : void
//Sortie : int, retourne -1 si vérrouillé et 0 sinon
int estVerrouille(){
char * verroup = verrouPath();
  FILE * file = fopen(verroup,"w");
  if (file == NULL) {
			perror("Failed to open file : verrou");
			exit (1);
	}
  int fd = fileno(file);
  int resultat=lockf(fd,F_TEST,0);
  free(verroup);
  fclose(file);
  return resultat;
}

//Renvoie le chemin du fichier dJob
//Entrée : String, identificateur du Job
//Sortie : String, chemin de dJob
char * pathD (char * s){
  char * resultat = malloc(1000*sizeof(char));
	strcpy(resultat,PROJETSE);
  strcat(resultat,"/d");
  strcat(resultat,s);
  return resultat;
}

//Renvoie le chemin du fichier jJob
//Entrée : String, identificateur du Job
//Sortie : String, chemin de jJob
char * pathJ (char * s){
  char * resultat = malloc(1000*sizeof(char));
	strcpy(resultat,PROJETSE);
  strcat(resultat,"/j");
  strcat(resultat,s);
  return resultat;
}

//Récupère les informations dans le fichier jJob
//Entrée : fichier jJob contenant les informations et ligne à extraire
//Sortie : String de la ligne extraite
char * getInfo(char * filepath, int line){
    int end, loop;
    char str[512];

    FILE *fd = fopen(filepath, "r");
    if (fd == NULL) {
        perror("Failed to open file : jJob");
        exit (1);
    }

    for(end = loop = 0;loop<line;++loop){
        if(0==fgets(str, sizeof(str), fd)){//include '\n'
            end = 1;//can't input (EOF)
            break;
        }
    }
    if(!end)
    fclose(fd);
		char * resultat=malloc(1000*sizeof(char));
		strcpy(resultat,str);
    size_t len = strlen(resultat);
    while (len > 0){
      if (resultat[len] == '\n') {
        resultat[len] = '\0';
      }
      len--;
    }
    return resultat;
}

//Renvoie le nom du fichier jJob
//Entrée : String, identificateur
//Sortie : String, nom fichier jJob
char * idJ (char * s){
  char * resultat = malloc(1000*sizeof(char));
	strcpy(resultat,"j");
  strcat(resultat,s);
  return resultat;
}

//Vérifie si le Spooler nous appartient
//Entrée : void
//Sortie : int, 1 si oui et 0 sinon
int propSpooler (){
  struct stat info;
	stat(PROJETSE, &info);
  return (info.st_uid==getuid());
}

//Fonction principale retirer
//Entrée : String, identificateur du fichier à retirer
//Sortie : void
void retirer(char * idRemove){
  static DIR * directory;
  static struct dirent *lecture;
	directory = opendir(PROJETSE);
  int existe = 0;

  while ((lecture = readdir(directory))) { //parcours le Spooler
    if(strcmp(lecture->d_name, ".") != 0 && strcmp(lecture->d_name, "..") != 0){
      char * idjob=idJ(idRemove);
      if(strcmp(lecture->d_name,idjob)==0){
				char * pathj = pathJ(idRemove); //Chemin du ficher jJob
        char * pathd = pathD(idRemove); //Chemin du ficher dJob
        char * getInfo1 = getInfo(pathj,1); //Récupère l'utilisateur du Job

				//Test si le Job nous appartient ou si on est le propriétaire du Spooler
        if((strcmp(getlogin(),getInfo1)==0)||(propSpooler())){
          existe = 1;

          remove(pathd); //Supprime dJob
          remove(pathj);	//Supprime jJob

					//printf("%s supprimé !\n",idRemove);
        }
        else{
          perror("Ce job ne vous appartient pas!");
          exit(1);
        }
				free(getInfo1);
        free(pathd);
        free(pathj);
      }
      free(idjob);
    }
  }
    closedir(directory);
  if(!existe){
    fprintf (stderr, "%s n'existe pas !\n", idRemove);
    exit(1);
  }
}

int main(int argc, char const *argv[]) {
	defineSpooler(); //On redéfinit le chemin du Spooler en cas de variable d'environnement PROJETSE
	int i;
  if(argc==1){
    fprintf (stderr, "Veuillez insérer un argument.\n");
    exit(1);
	}
  char * idRemove = malloc(1000*sizeof(char)); //On Stock l'identificateur du job à supprimer
  for (i=1;i<=argc-1;i++){
		strcpy(idRemove,argv[i]);
    while (estVerrouille()==-1){ //Vérifie si le Spooler est verrouillé
      printf("Le Spooler est verouillé ! \n");
      sleep(3);
    }
    verrouille(); //Verrouille le fichier verrou
    retirer(idRemove); //Retire le Job du Spooler
    deverrouille(); //Déverrouille le fichier verrou
  }
  free(idRemove);
  return 0;
}
