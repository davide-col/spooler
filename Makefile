CC = gcc
CCFLAGS = -Wall -g

all: demon deposer retirer lister

demon: demon.c
	$(CC) $(CCFLAGS) -o demon demon.c
	chmod 4755 demon

deposer: deposer.c
	$(CC) $(CCFLAGS) -o deposer deposer.c
	chmod 4755 deposer

retirer: retirer.c
	$(CC) $(CCFLAGS) -o retirer retirer.c
	chmod 4755 retirer

lister: lister.c
	$(CC) $(CCFLAGS) -o lister lister.c
	chmod 4755 lister

clean:
	rm demon deposer retirer lister

update: clean all
