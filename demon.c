#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <getopt.h>
#include <libgen.h>
#include <time.h>
#include <sys/wait.h>
#define SPOOLER "./Spooler"

char * PROJETSE;
int signal_kill = 0;

//Définit le chemin du Spooler
//Entrée : void
//Sortie : void
void defineSpooler(){
  if (getenv("PROJETSE")==NULL){
    PROJETSE=SPOOLER;
  }
  else{
    PROJETSE=getenv("PROJETSE");
  }
}

//Récupère les informations dans le fichier jJob
//Entrée : fichier jJob contenant les informations et ligne à extraire
//Sortie : String de la ligne extraite
char * getInfo(char * filepath, int line){
    int end, loop;
    char str[512];

    FILE *fd = fopen(filepath, "r");
    if (fd == NULL) {
        perror("Failed to open file : jJob");
        exit (1);
    }

    for(end = loop = 0;loop<line;++loop){
        if(0==fgets(str, sizeof(str), fd)){//include '\n'
            end = 1;//can't input (EOF)
            break;
        }
    }
    if(!end)
    fclose(fd);
		char * resultat=malloc(1000*sizeof(char));
		strcpy(resultat,str);
    size_t len = strlen(resultat);
    while (len > 0){
      if (resultat[len] == '\n') {
        resultat[len] = '\0';
      }
      len--;
    }
    return resultat;
}

//Supprimme les saut de lignes (/n) d'un string
//Entrée : string que l'on souhaite modifier
//Sortie : string modifié
char * deleteN (char * str){
  char * resultat=str;
  size_t len = strlen(resultat);
  while (len > 0){
    if (resultat[len] == '\n') {
      resultat[len] = '\0';
    }
    len--;
  }
  return resultat;
}

//Ecrit les informations dans le fichier jJob
//Entrée : chemin du fichier jJob, string qui contient le text
//Sortie : void
void putInfo (const char *filepath, const char *data){
  FILE *fp = fopen(filepath, "ab");
  if (fp == NULL) {
      perror("Failed to open file : jJob");
      exit (1);
  }
  if (fp != NULL)
  {
      fputs(data, fp);
      fclose(fp);
  }
}

//Verifie l'existance du Spooler et le crée sinon
//Entree : void
//Sortie : int qui renvoie 1 s'il existe, 0 sinon
int existanceSpooler (){
  DIR* directory = opendir(PROJETSE);
  if (directory){
      closedir(directory);
      return 1;
  }
  else if (ENOENT == errno){ //Le repertoire n'existe pas
      int val = mkdir(PROJETSE,0700);
      if (val==-1){
        fprintf (stderr, "Failed to open (or create) Spooler file : %s\n", PROJETSE);
        exit(1);
      }
      return 0;
  }
  else{ //opendir a échoué
    fprintf (stderr, "Failed to open (or create) Spooler file : %s\n", PROJETSE);
      exit(1);
  }
}

//Crée le fichier Sommaire s'il existe pas
//Le fichier Sommaire va contenir les informations des opérations du demon
//Entrée : fichier text Sommaire
//Sortie : void
void creationSommaire (const char * fichier){
  FILE * fileFichier = fopen(fichier, "w+");
  if (fileFichier == NULL) {
      fprintf (stderr, "Failed to create log file : %s\n", fichier);
      exit (1);
  }
  time_t now;
	struct tm *curtime = localtime(&now);
  fprintf(fileFichier, "Starting at : %s",asctime(curtime));
	fclose(fileFichier);
}

//Retourne le chemin du fichier verrou
//Entrée : void
//Sortie : String
char * verrouPath(){
	char * resultat=malloc(1000*sizeof(char));
	strcpy(resultat,PROJETSE);
	strcat(resultat,"/verrou");
	return resultat;
}

//Verrouille le fichier verrou
//Entrée : void
//Sortie : void
void verrouille(){
  char * verroup = verrouPath();
  FILE * file = fopen(verroup,"w");
  if (file == NULL) {
			perror("Failed to open file : verrou");
			exit (1);
	}
  int fd = fileno(file);
  lockf(fd,F_TLOCK,0);
  fclose(file);
  free(verroup);
	return;
}

//Déverrouille le fichier verrou
//Entrée : void
//Sortie : void
void deverrouille(){
  char * verroup = verrouPath();
  FILE * file = fopen(verroup,"w");
  if (file == NULL) {
			perror("Failed to open file : verrou");
			exit (1);
	}
  int fd = fileno(file);
  lockf(fd,F_ULOCK,0);
  fclose(file);
  free(verroup);
  return;
}

//Test si le fichier verrou est vérrouillé
//Entrée : void
//Sortie : int, retourne -1 si vérrouillé et 0 sinon
int estVerrouille(){
char * verroup = verrouPath();
  FILE * file = fopen(verroup,"w");
  if (file == NULL) {
			perror("Failed to open file : verrou");
			exit (1);
	}
  int fd = fileno(file);
  int resultat=lockf(fd,F_TEST,0);
  free(verroup);
  fclose(file);
  return resultat;
}

//Vérifie l'existance du fichier verrou, création sinon
//Entrée : void
//Sortie : void
void existanceVerrou(){
  char * verroup = verrouPath();
	FILE * fileVerrou = fopen(verroup, "ab+");
  if (fileVerrou == NULL) {
			perror("Failed to create file : verrou");
			exit (1);
	}
	fclose(fileVerrou);
  free(verroup);
}

//Renvoie le chemin du jJob à partir du chemin dJob
//Entrée : chemin dJob
//Sortie : chemin jJob
char * d2jJob(char * dJobPath){
	char * name = malloc(1000*sizeof(char));
  strcpy(name,basename(dJobPath));
	char * jJobPath = malloc(1000*sizeof(char));
	name[0]='j';
  strcpy(jJobPath,PROJETSE);
	strcat(jJobPath,"/");
	strcat(jJobPath,name);
  free(name);
  return jJobPath;
}

//Renvoie le chemin d'un fichier dans le Spooler
//Entrée : String, nom du fichier
//Sortie : String, chemin du fichier
char * path (char * s){
  char * resultat = malloc(1000*sizeof(char));
  strcpy(resultat,PROJETSE);
  strcat(resultat,"/");
  strcat(resultat,s);
  return resultat;
}

//Renvoie l'ID d'un fichier
//Entrée : String, chemin du fichier
//Sortie : int, ID du fichier
int fileID (char * file){
  struct stat info;
  stat(file, &info);
  return info.st_uid;
}

//Déplace le fichier (commande move)
//Ceci va nous permettre de rennomer le fichier archiver à son nom initial
//Entrée : String de départ, String d'arrivée
//Sortie : void
void mv (char * entree, char * sortie){
  int pid = fork();
  if (pid < 0){
    perror("Echec du clonage.");
    exit(1);
  }
  if (pid > 0){
    int status;
    waitpid(pid, &status, 0);
  }
  else {
    char * args[]={"/bin/mv",entree,sortie,NULL};
    execvp("/bin/mv",args);
  }
}

//Renvoie le chemin où le fichier archivé doit être déposé
//Entrée : String, chemin du fichier jJob
//Sortie : String, chemin d'arrivée pour le fichier compressé
char * destPathGZ(char * jJobPath){
  char * getName = getInfo(jJobPath,4);
  char * destpathgz = malloc(1000*sizeof(char));
  char * jJobPath2 = malloc(1000*sizeof(char));
  strcpy(jJobPath2,jJobPath);
  strcpy(destpathgz,dirname(jJobPath2));
  strcat(destpathgz,"/");
  strcat(destpathgz,getName);
  strcat(destpathgz,".gz");
  free(jJobPath2);
  free(getName);
  return destpathgz;
}

//Compresse le fichier dJob avec le nom initial dans le Spooler
//Entrée : chemin du fichier jJob, dJob et chemin final du fichier compressé (destpathgz)
//Sortie : void
void monGzip(char * jjobpath, char * djobpath, char * destpathgz){
  char * djobpathgz = malloc(1000*sizeof(char));
  strcpy(djobpathgz,djobpath);
  strcat(djobpathgz,".gz");
  int pid = fork();
  if (pid < 0){
    perror("Echec du clonage.");
    exit(1);
  }
  if (pid > 0){
    int status;
    waitpid(pid, &status, 0);
    mv(djobpathgz,destpathgz);
  }
  else {
    char * execString = malloc(1000*sizeof(char));
    strcpy(execString,djobpath);
    char * args[]={"/bin/gzip","-n",djobpath, NULL};
    free(execString);
    execvp("/bin/gzip",args);
  }
  free(djobpathgz);
}

//Test si le fichier jJob existe
//Entrée : chemin supposé de jJob
//Sortie : int, 1 s'il existe et 0 sinon
int jobExiste (char * job){
  FILE * file = fopen(job,"r");
  if (file == NULL) {
    return 0;
	}
  else return 1;
}

//Arrête le demon proprement en cas de signal kill -15
void signalArret(int numsig){
  signal_kill=1;
}

//Fonction qui va parcourir les fichiers à compresser dans le Spooler
//Entrée : String, chemin du fichier Sommaire (contenant le descriptif des compressions)
//Sortie : void
void compress(const char * fichier){


  static DIR * directory;
  static struct dirent *lecture;
	directory = opendir(PROJETSE);
  while ((lecture = readdir(directory))) { //parcours le repertoire courant
    if(strcmp(lecture->d_name, ".") != 0 && strcmp(lecture->d_name, "..") != 0){
      char * djobpath = path(lecture->d_name); //Chemin dJob
      char * jjobpath = d2jJob(djobpath); //Chemin jJob

      //Véreifie s'il existe des fichiers à compresser
      if((lecture->d_name[0]=='d')&&(jobExiste(jjobpath))){
            char * destpathgz = destPathGZ(jjobpath); //Chemin destination pour la compression
            //Récupération des informations dans le fichier jJob
            char * get1=getInfo(jjobpath,1);
            char * get2=getInfo(jjobpath,2);
            char * get3=getInfo(jjobpath,3);
            char * get4=getInfo(jjobpath,4);
            char * get6=getInfo(jjobpath,6);
            time_t now;
          	struct tm *curtime = localtime(&now);

            printf("Compression de : %s\n",get4);
            monGzip(jjobpath,djobpath,destpathgz); //Compression du fichier
            printf("Compression ok !\n");

						struct stat statbuf;
	          int fd = stat(destpathgz, &statbuf);
	          if (fd < 0){
	            perror("Echec d'ouverture de l'archive créée");
	            exit(0);
	          }

            //Ecriture des informaitons dans le fichier Sommaire
	          FILE *fFILE = fopen(fichier,"a");
            if (fFILE == NULL) {
          			perror("Failed to open file : log file");
          			exit (1);
          	}
	          fprintf(fFILE, "\nid=%s orgdate=%s user=%s file=%s orgsize=%s\n\tcurdate=%s gzipsize=%ld"
            ,get2,get6,get1,get4,get3,deleteN(asctime(curtime)),statbuf.st_size);

            //Suppression du fichier jJob
            int test = remove(jjobpath);
            if(test<0){
              perror ("Echec du remove");
            }
            free(destpathgz);
            free(get1);
            free(get2);
            free(get3);
            free(get4);
            free(get6);
	          fclose(fFILE);
      }
      free(djobpath);
      free(jjobpath);
    }
  }
    closedir(directory);
}

//Fonction principale demon qui va boucler sans s'arrêter
//Entrée : les options entrées (getopt), String du fichier Sommaire
//Sortie : void
void demon (int debug, int foreground, int sec, const char * fichier){
  if(foreground==0){ //Mise en arrière plan si l'option foreground n'est pas activée
    int pid = fork();
    if (pid < 0){
      perror("Echec du clonage.");
      exit(1);
    }
    if (pid > 0){
      //on tue le père
      //à embelir
      exit(0);
    }
    else {
        //le corps du demon
        while (!signal_kill){
          printf("Recherche ...\n");
          while (estVerrouille()==-1){ //Attente si Spooler vérrouillé
      			printf("Le Spooler est verouillé ! \n");
      			sleep(3);
      		}
          verrouille(); //Verrouille le Spooler
          compress(fichier); //Lance le parcours du Spooler
          deverrouille(); //Déverrouille le Spooler
          sleep(sec); //Attends quelques secondes
        }
    }
  }
  else { //Si foreground activé pas de mise en arrière plan
    while (!signal_kill){
      printf("Recherche ...\n");
      while (estVerrouille()==-1){ //Attente si Spooler vérrouillé
  			printf("Le Spooler est verouillé ! \n");
  			sleep(3);
  		}
      verrouille(); //Verrouille le Spooler
      compress(fichier); //Lance le parcours du Spooler
      deverrouille(); //Déverrouille le Spooler
      sleep(sec); //Attends quelques secondes
    }
  }
}

int main (int argc, char * argv[]) {
  signal(SIGTERM, signalArret);
  int dflag = 0;
  int fflag = 0;
  int sec = 3;
  int c;
  const char * fichier = argv[argc-1]; //Sauvegarde du chemin du fichier entré (Sommaire)
  while ((c = getopt (argc, argv, "fdi:"))!=-1){ //Recherche des options avec getopt
      switch (c){
        case 'f':
          fflag = 1;
          break;
        case 'i':
          sec = atoi(optarg);
          break;
        case 'd':
          dflag = 1;
          break;
        case '?':
          if (optopt == 'i'){
            fprintf (stderr, "Option -%c requires an argument.\n", optopt);
            exit(1);
            break;
          }
          else {
            fprintf(stderr, "Unknown option -%c.\n",optopt);
            exit(1);
            break;
          }
        default:
          fprintf (stderr, "getopt\n");
          exit(1);
      }
  }
  //Redirection de la sortie standard dans /dev/null si l'option -d est désactivée
  if (dflag==0){
    int new;
    fflush(stdout);
    new = open("/dev/null", O_WRONLY);
    dup2(new, 1);
    close(new);
  }
  defineSpooler(); //On redéfinit le chemin du Spooler en cas de variable d'environnement PROJETSE
  existanceSpooler(); //Vérification de l'existance du Spooler, création sinon
  existanceVerrou(); //Vérification de l'existance du verrou, création sinon
  creationSommaire(fichier); //Vérification de l'existance de Sommaire, création sinon
  demon(dflag,fflag,sec,fichier); //Execution du demon
  return 0;
}
